﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using NUnit;
using NUnit.Framework;
using OpenQA.Selenium.Interactions;

namespace ConsoleApplication6
{


   [TestFixture]
     public class Kino
        {
        public static IWebDriver driver { get; private set; }
       [OneTimeSetUp]
        public void InitializerDriver() {
          ChromeOptions options = new ChromeOptions();
           options.AddArgument("start-maximized");
           options.LeaveBrowserRunning = true;
           options.AddUserProfilePreference("intl.accept_languages","nl");
            IWebDriver driver = new ChromeDriver(options); }
            
        [SetUp]

       public void OpenMainPage()
        
        {
            IWebDriver driver = new ChromeDriver();
            driver.Url = "https://www.kinopoisk.ru/";
          //  driver.Navigate().GoToUrl("https://www.kinopoisk.ru/");
           // driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(50));
        }
        [TearDown]
        public static void CloseDriver()
        {
            // IWebDriver driver = new ChromeDriver();
            driver.Close();
            // driver.Quit();
        }
     //   [TestCase("Осьминог")]
        // 
            [Test]
        public void TestingMethod(string movie)
        {
          //  IWebDriver driver = new ChromeDriver();
          //  driver.Url = "https://www.kinopoisk.ru/";
            IWebElement field = driver.FindElement(By.XPath("//header//form/input[@name='kp-query']"));
            field.SendKeys("осьминог");
            IWebElement btn = driver.FindElement(By.XPath("//*[@data-reactid='82']"));
            btn.Click();
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromMinutes(1));

            Func<IWebDriver, bool> waitForElement = new Func<IWebDriver, bool>((IWebDriver Web) =>
            {
                Console.WriteLine(Web.FindElement(By.ClassName("search_results_top")));//.GetAttribute("innerHTML"));
                return true;
            });
            wait.Until(waitForElement);

            IWebElement link = driver.FindElement(By.Id("FlappImg_3"));
            link.Click();
            Func<IWebDriver, bool> waitForElement1 = new Func<IWebDriver, bool>((IWebDriver Web) =>
            {
                Console.WriteLine(Web.FindElement(By.Id("headerFilm")));//.GetAttribute("innerHTML"));
                return true;
            });
            wait.Until(waitForElement1);
            Console.WriteLine(driver.Title);
            IWebElement maybe = driver.FindElement(By.XPath("//p[@text='Скорее всего, вы ищете:']"));
            //Возможно вы ищете
            Assert.True(maybe.Displayed);
            IWebElement element = driver.FindElement(By.XPath("//div[contains(@itemprop,'description')]"));
            string linkText = element.Text;
            Console.WriteLine(linkText);
            char[] charArr = linkText.ToCharArray();
            byte[] byteArr = new byte[50000];
            try
            {
                System.IO.FileStream fs = new FileStream("2.txt", FileMode.OpenOrCreate, FileAccess.Write);

                Encoder enc = Encoding.UTF8.GetEncoder();
                enc.GetBytes(charArr, 0, charArr.Length, byteArr, 0, true); // перекодирование
                fs.Write(byteArr, 0, byteArr.Length); // запись массива байт
                fs.Dispose(); // освобождаем ресурсы
            }
            catch (Exception e)
            {
                Console.WriteLine("Error " + e.Message);
                return;
            }

        }
            //--------------------------------------------------------------------------------------
            /////////////////////////////////////////////////////////////////////////////////////////
      //      [TestCase("Кактус")]
            [Test]
            public void TestingMethod1(string movie1)
        {
           // IWebDriver driver = new ChromeDriver();
            driver.Url = "https://www.kinopoisk.ru/";
            IWebElement field = driver.FindElement(By.XPath("//input[@name='kp-query']"));
            field.SendKeys("Кактус");
            IWebElement btn = driver.FindElement(By.XPath("//*[@data-reactid='82']"));
            btn.Click();
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromMinutes(1));

            Func<IWebDriver, bool> waitForElement = new Func<IWebDriver, bool>((IWebDriver Web) =>
            {
                Console.WriteLine(Web.FindElement(By.ClassName("search_results_top")));//.GetAttribute("innerHTML"));
                return true;
            });
            wait.Until(waitForElement);
            IWebElement maybe = driver.FindElement(By.XPath("//p[@text='Скорее всего, вы ищете:']"));
            //Возможно вы ищете
            Assert.True(maybe.Displayed);

            IWebElement link = driver.FindElement(By.Id("FlappImg_0"));
            link.Click();
            Func<IWebDriver, bool> waitForElement1 = new Func<IWebDriver, bool>((IWebDriver Web) =>
            {
                Console.WriteLine(Web.FindElement(By.Id("headerFilm")));//.GetAttribute("innerHTML"));
                return true;
            });
            wait.Until(waitForElement1);
            Console.WriteLine(driver.Title);
           
            IWebElement element = driver.FindElement(By.XPath("//div[contains(@itemprop,'description')]"));
            string linkText = element.Text;
            Console.WriteLine(linkText);
            char[] charArr = linkText.ToCharArray();
            byte[] byteArr = new byte[50000];
            try
            {
                System.IO.FileStream fs = new FileStream("2.txt", FileMode.OpenOrCreate, FileAccess.Write);

                Encoder enc = Encoding.UTF8.GetEncoder();
                enc.GetBytes(charArr, 0, charArr.Length, byteArr, 0, true); // перекодирование
                fs.Write(byteArr, 0, byteArr.Length); // запись массива байт
                fs.Dispose(); // освобождаем ресурсы
            }
            catch (Exception e)
            {
                Console.WriteLine("Error " + e.Message);
                return;
            }
            // driver.Quit();
            driver.Navigate().Back();
        }
       [Test]
    /*   public void logIn()
        {
            driver.FindElement(By.XPath("//a[text()='Войти на сайт']")).Click();
            driver.SwitchTo().Frame("kp2-authapi-iframe");
            IWebElement loginField = driver.FindElement(By.XPath("//input[@name='login']"));
            loginField.SendKeys("qwerty");
            Assert.True(loginField.Text == "qwerty");
        }*/
        public void logIn()
            
        {
            Actions action = new Actions(driver);
          IWebElement someElement= driver.FindElement(By.XPath("//a[text()='Войти на сайт']"));
          action.ContextClick(someElement).Perform();
          action.Click(someElement).Perform();
            driver.SwitchTo().Frame("kp2-authapi-iframe");
            IWebElement loginField = driver.FindElement(By.XPath("//input[@name='login']"));
            loginField.SendKeys("qwerty");
            Assert.True(loginField.Displayed);
            Actions action1 = new Actions(driver);
            IWebElement someElement1 = driver.FindElement(By.XPath("//a[text()='Войти на сайт']"));
            action.ContextClick(someElement1).Perform();
            action.Click(someElement1).Perform();
            driver.SwitchTo().Frame("kp2-authapi-iframe");
            IWebElement passField = driver.FindElement(By.XPath("//input[@name='password']"));
            passField.SendKeys("qwerty");
           
        }

       
    }
    }
