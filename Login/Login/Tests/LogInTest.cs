﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System.IO;
using System.Threading;
using NUnit;
using NUnit.Framework;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System.Data.OleDb;
using System.Configuration;
using ExcelDataReader;
using Login.Utils;
using Login.Pages;

namespace Login.Tests
{
    [TestFixture]
  //  [Parallelizable]
    class LogInTest
    {
        MainPage mainPage;
       private const string CORRECT_EMAIL = "reversija@tut.by";
        private const string CORRECT_PASSWORD = "qwerty6543";

         [SetUp]
       public void OpenMainPage()
        {
          mainPage= new MainPage();

        }
       
         [Test]
   //      [Parallelizable]
         public void LogInPossitive ()
         {
             bool isLogInSuccessful = mainPage.Login(CORRECT_EMAIL, CORRECT_PASSWORD, false);
             Assert.True(isLogInSuccessful);
             mainPage.LogOut();
         }

         [Test]
        public void CheckIsEmailTyped()
         {
             mainPage.fillEmailField(CORRECT_EMAIL);
             mainPage.getEmailField();
             Assert.AreEqual(CORRECT_EMAIL, mainPage.getEmailField());
         }
         [Test]
       /* [Parallelizable]
        [TestCase("reversija@tut.by", "sdfgtr", false)]
        [TestCase("sdfgtres", "qwerty6543", false)]
         public void LogInNegative(string email, string password, bool checkFailure)
         {
             bool isIncorrectCredentialsMessageAppear = mainPage.Login(email, password, checkFailure);
             Assert.True(isIncorrectCredentialsMessageAppear);
         }*/

         [TestCase("NegativeCase1")]
        [TestCase("NegativeCase2")]
        public void LogInNegative(string testCase)
         {
          // bool isIncorrectCredentialsMessageAppear = mainPage.Login(email, password, true);
             var userData = ExcelDataAccess.GetTestData(testCase);
             bool isIncorrectCredentialsMessageAppear = mainPage.Login(userData.UserName, userData.Password, true);
             Assert.True(isIncorrectCredentialsMessageAppear);
         }
       
         [TearDown]
         public void CloseDriver()
         {
             mainPage.CloseBrowser();
         }

    }
}
