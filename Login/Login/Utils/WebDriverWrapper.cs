﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Extensions;
using System.IO;
using System.Threading;
using NUnit;
using NUnit.Framework;
using OpenQA.Selenium.Interactions;
using NLog;
using OpenQA.Selenium.Support.Events;
using OpenQA.Selenium.Remote;
//using Login.Pages;

namespace Login.Utils
{
    public class WebDriverWrapper : ChromeDriver // FirefoxDriver//
    {
        

        private static Logger logger = LogManager.GetCurrentClassLogger();


     //   public WebDriverWrapper()//(IWebDriver webDriver)
      //  {
            //webDriverWrapper = new WebDriverWrapper();
            // this.driver = webDriver;

     //   }
       
            public void ClickByXpath(string xpath, int timeout)
            {
                logger.Debug("Search for element with xpath: " + xpath);
                WaitForElementClickable(xpath, timeout);
                IWebElement elem = FindElement(By.XPath(xpath));
                logger.Debug("Click element with xpath: " + xpath);
                elem.Click();
            }
            public void Wait(int timeout)
            {
                WebDriverWait wait = new WebDriverWait(this, TimeSpan.FromMinutes(timeout));

            }
            public void WaitForElementVisible(string xpath, int timeout)
            {
                //   IWebDriver   wait=   driver.Manage().Timeouts().ImplicitWait(TimeSpan.FromSeconds(timeout));
                //IWebDriver wait1 = driver.Manage().Timeouts(TimeSpan.FromSeconds(timeout))//.(TimeSpan.FromSeconds(timeout));

                WebDriverWait wait = new WebDriverWait(this, TimeSpan.FromSeconds(timeout));
                wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(xpath)));

            }

            public void WaitForElementClickable(string xpath, int timeout)
            {
                WebDriverWait wait = new WebDriverWait(this, TimeSpan.FromSeconds(timeout));
                wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(xpath)));
            }

            internal static IWebDriver WaitForElementVisible()
            {
                throw new NotImplementedException();
            }
        }
    }
