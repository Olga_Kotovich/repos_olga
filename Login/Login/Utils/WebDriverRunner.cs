﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System.IO;
using System.Threading;
//using NUnit;
//using NUnit.Framework;
using OpenQA.Selenium.Interactions;
//using UnitTestProject1.Utils.WebDriverWrapper;
using Login.Pages;
using NLog;

namespace Login.Utils
{
    class WebDriverRunner
    {
       public static IWebDriver driver;// { get; set; }
     
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static int defaultTimeoutSeconds = 60;
      public WebDriverRunner()
        {
          //  driver = new ChromeDriver();
         // driver = new WebDriverWrapper();
           // driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(50));
           // WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromMinutes(1));
            WebDriverFactory factory = new WebDriverFactory();
           driver=  factory.CreateDriver(Config.BrowserType);
          //  driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(defaultTimeoutSeconds);
            logger.Debug("Создаем сущность webDriver");
            logger.Trace("Устанавливаем время ожидания {0} секунд"+ defaultTimeoutSeconds);
           
        }
        public static IWebDriver GetDriver()
        {
           /* if (driver==null)
            {
                new WebDriverRunner();
            }*/
            new WebDriverRunner();
            return driver;
        }
     /*   public IWebDriver CreateDriver(string browserType)
        {
            switch (browserType)
            {
            case "Chrome":
            return new ChromeDriver();
            case "Firefox":
            return new FirefoxDriver();
                default:
                return null;
            }
        }*/
    }
}
