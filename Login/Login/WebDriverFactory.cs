﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Extensions;
using System.IO;
using System.Threading;
using NUnit;
using NUnit.Framework;
using OpenQA.Selenium.Interactions;
using NLog;
using OpenQA.Selenium.Support.Events;
using OpenQA.Selenium.Remote;

namespace Login
{
   public class WebDriverFactory
    {
        private static IWebDriver driver;// { get; set; }
        EventFiringWebDriver firingDriver = null;
        public IWebDriver CreateDriver(String browserType)
        {
            //  DesiredCapabilities capabilities = new DesiredCapabilities();
            switch (browserType)
            {
                case "Chrome":
                    /*    capabilities =  DesiredCapabilities.Chrome();
                        capabilities.SetCapability(CapabilityType.BrowserName, "chrome");*/
                    // return new ChromeDriver();
                    firingDriver = new EventFiringWebDriver(new ChromeDriver());
                    break;
                case "Firefox":
                    /*   capabilities = DesiredCapabilities.Firefox();
                       capabilities.SetCapability(CapabilityType.BrowserName, "firefox");*/
                    // return new FirefoxDriver();
                    firingDriver = new EventFiringWebDriver(new FirefoxDriver());
                    break;
                default:
                    // return null;
                    break;
            }
            /*  capabilities.SetCapability(CapabilityType.Platform, new Platform(PlatformType.Any));
              driver = new RemoteWebDriver(new Uri("http://localhost:4444/wd/hub"), capabilities);*/
            // driver = new RemoteWebDriver(new Uri("http://localhost:4444/wd/hub"), DesiredCapabilities.Chrome());
            //  return driver;

            firingDriver.ExceptionThrown += firingDriver_TakesScreenshotOnException;//(driver, );
            driver = firingDriver;
            return driver;
        }
        private void firingDriver_TakesScreenshotOnException(object sender, WebDriverExceptionEventArgs e)
        {
            firingDriver.TakeScreenshot().SaveAsFile("Exception-" + ".png");

        }
    }
}
