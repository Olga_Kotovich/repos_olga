﻿/*using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System.IO;
using OpenQA.Selenium.Support.PageObjects;
using System.Runtime.InteropServices;
using System.Threading;
using NUnit;
using NUnit.Framework;
using OpenQA.Selenium.Interactions;
using UnitTestProject1.Utils;
using UnitTestProject1.Tests;
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;
using NUnit.Framework;
using Login.Utils;
using OpenQA.Selenium.Chrome;

namespace Login.Pages
{
   public class MainPage : BasePage
    {
       private  WebDriverWrapper webDriverWrapper;
      public  string url = "https://www.kinopoisk.ru/";
      private const string LOGIN_BUTTON_XPATH = "//header/div/span/span/a ";  //a[text()='Войти на сайт'";
      private const string AUTH_FRAME_XPATH = "//iframe[@name='kp2-authapi-iframe']";
      private const string FRAME_HEADER_TEXT_XPATH = "//body/div/div[1]"; //div[contains(@text(),'Вход на КиноПоиск']"; 
      private const string NAME_FIELD_XPATH="//input[@name='login']";
      private const string PASSWORD_FIELD_XPATH="//input[@name='password']";
      private const string ENTER_BUTTON_XPATH = "//html/body/div/div[2]/div/form/div[1]/div[1]/button"; //"//form/div[1]/div[1]/button";
      private const string FAILURE_TEXT_XPATH="//div[text()='Вы ошиблись в почте или пароле']";
      private const string LOGOUT_BUTTON_XPATH = ".//*[@id='partial_component__header']/div/header/div/span/a";
   
    
             public  MainPage()
             {
                driver.Url = url;
             }
     
        
             public bool Login(string emailField, string password, bool checkFailure)
       {
           webDriverWrapper = new WebDriverWrapper();
           fillEmailField(emailField);
           driver.FindElement(By.XPath(PASSWORD_FIELD_XPATH)).SendKeys(password);
         //  webDriverWrapper.Wait(2);
          // WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromMinutes(1));
          // wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(ENTER_BUTTON_XPATH)));
          // webDriverWrapper = new WebDriverWrapper();
           webDriverWrapper.WaitForElementClickable(ENTER_BUTTON_XPATH, 120);
          
           driver.FindElement(By.XPath(ENTER_BUTTON_XPATH)).Click();
         //  WebDriverWait wait1 = new WebDriverWait(driver, TimeSpan.FromMinutes(1));
          
          // wait1.Until(ExpectedConditions.ElementToBeClickable(By.XPath(LOGOUT_BUTTON_XPATH)));
          
           if (checkFailure)
               return driver.FindElement(By.XPath(LOGOUT_BUTTON_XPATH)).Displayed;
           else
               return driver.FindElement(By.XPath(FAILURE_TEXT_XPATH)).Displayed;
       }
       
       public MainPage fillEmailField(string emailField)
       {
           driver.FindElement(By.XPath(LOGIN_BUTTON_XPATH)).Click();
         
           webDriverWrapper.Wait(2);
          // WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromMinutes(1));
           driver.SwitchTo().Frame(driver.FindElement(By.XPath(AUTH_FRAME_XPATH)));
         //   WebDriverWait wait1 = new WebDriverWait(driver, TimeSpan.FromMinutes(1));
          
           webDriverWrapper.Wait(2);
         //  webDriverWrapper.WaitForElementVisible(FRAME_HEADER_TEXT_XPATH, 120);
           driver.FindElement(By.XPath(NAME_FIELD_XPATH)).SendKeys(emailField);
           webDriverWrapper.Wait(20);
          // WebDriverWait wait2 = new WebDriverWait(driver, TimeSpan.FromMinutes(1));
           return this;
       }

       public string getEmailField()
       {
           return driver.FindElement(By.XPath(NAME_FIELD_XPATH)).Text;
       }
       public bool LogOut()
       {
           driver.FindElement(By.XPath(LOGOUT_BUTTON_XPATH)).Click();
           return driver.FindElement(By.XPath(LOGOUT_BUTTON_XPATH)).Displayed;
       }
    }
}
