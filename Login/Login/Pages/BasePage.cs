﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;
using System.IO;
//using System.Runtime.InteropServices;
using System.Threading;
using NUnit;
using NUnit.Framework;
using Login.Utils;

namespace Login.Pages
{
   public class BasePage
    {
        protected IWebDriver driver;
       
        public BasePage()
        {
            driver = WebDriverRunner.GetDriver();
          //   webDriverWrapper = new WebDriverWrapper(); //(driver);
        }
     /*   public void Wrapper()
        {
            webDriverWrapper = new WebDriverWrapper();
          
        }*/
       
        public void CloseBrowser()
        {
            driver.Close();
            driver.Quit();
        }

    }
}
